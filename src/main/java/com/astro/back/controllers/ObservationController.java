package com.astro.back.controllers;

import com.astro.back.entities.Observation;
import com.astro.back.entities.dtos.ObservationPropertiesDto;
import com.astro.back.services.interfaces.ObservationService;
import com.google.gson.Gson;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.Point;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/observation")
public class ObservationController {

    @Autowired
    private ObservationService observationService;

    @GetMapping
    List<Observation> getAll() {
        return observationService.findAll();
    }

    @GetMapping(value = "/featureCollection/{eventId}")
    String getFeatureCollection(@PathVariable Long eventId) {
        return FeatureCollection.fromFeatures(
                observationService.filter("event.id==" + eventId)
                        .stream()
                        .map(observation -> Feature.fromGeometry(
                                Point.fromLngLat(
                                        observation.getPosition().getLongitude(),
                                        observation.getPosition().getLatitude()),
                                new Gson().toJsonTree(new ObservationPropertiesDto(observation)).getAsJsonObject()
                        )).collect(Collectors.toList())
        ).toJson();
    }

    @GetMapping(value = "/{id}")
    Observation getById(@PathVariable Long id) {
        return observationService.findById(id);
    }

    @PostMapping
    ResponseEntity add(@Valid @RequestBody Observation observation) {
        Observation savedObservation = observationService.add(observation);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(savedObservation.getId())
                .toUri();

        return ResponseEntity.created(location).build();
    }

    @PutMapping(value = "/{id}")
    ResponseEntity update(@RequestBody Observation observation,
                          @PathVariable Long id) {
        observationService.update(observation, id);

        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(value = "/{id}")
    void delete(@PathVariable Long id) {
        observationService.delete(id);
    }

    @GetMapping(value = "/filter")
    List<Observation> filter(@RequestParam(value = "search") String query) {
        return observationService.filter(query);
    }
}
