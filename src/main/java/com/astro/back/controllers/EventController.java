package com.astro.back.controllers;

import com.astro.back.entities.Event;
import com.astro.back.entities.dtos.EventDto;
import com.astro.back.services.interfaces.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/event")
public class EventController {

    @Autowired
    private EventService eventService;

    @GetMapping
    List<Event> getAll() {
        return eventService.findAll();
    }

    @GetMapping(value = "/basic")
    List<EventDto> getAllBasic() {
        return eventService.findAllBasic();
    }

    @GetMapping(value = "/{id}")
    Event getById(@PathVariable Long id) {
        return eventService.findById(id);
    }

    @PostMapping
    ResponseEntity add(@Valid @RequestBody Event event) {
        Event savedEvent = eventService.add(event);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(savedEvent.getId())
                .toUri();

        return ResponseEntity.created(location).build();
    }

    @PutMapping(value = "/{id}")
    ResponseEntity update(@RequestBody Event event,
                          @PathVariable Long id) {
        eventService.update(event, id);

        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(value = "/{id}")
    void delete(@PathVariable Long id) {
        eventService.delete(id);
    }

    @GetMapping(value = "/filter")
    List<Event> filter(@RequestParam(value = "search") String query) {
        return eventService.filter(query);
    }
}
