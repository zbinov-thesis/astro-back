package com.astro.back.controllers;

import com.astro.back.entities.Tag;
import com.astro.back.services.interfaces.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/tag")
public class TagController {

    @Autowired
    private TagService tagService;

    @GetMapping
    List<Tag> getAll() {
        return tagService.findAll();
    }

    @GetMapping(value = "/{id}")
    Tag getById(@PathVariable Long id) {
        return tagService.findById(id);
    }

    @PostMapping
    ResponseEntity add(@Valid @RequestBody Tag tag) {
        Tag savedTag = tagService.add(tag);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(savedTag.getId())
                .toUri();

        return ResponseEntity.created(location).build();
    }

    @PutMapping(value = "/{id}")
    ResponseEntity update(@RequestBody Tag tag,
                          @PathVariable Long id) {
        tagService.update(tag, id);

        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(value = "/{id}")
    void delete(@PathVariable Long id) {
        tagService.delete(id);
    }

    @GetMapping(value = "/filter")
    List<Tag> filter(@RequestParam(value = "search") String query) {
        return tagService.filter(query);
    }
}
