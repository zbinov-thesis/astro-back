package com.astro.back.controllers;

import com.astro.back.entities.Position;
import com.astro.back.services.interfaces.PositionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/position")
public class PositionController {

    @Autowired
    private PositionService positionService;

    @GetMapping
    List<Position> getAll() {
        return positionService.findAll();
    }

    @GetMapping(value = "/{id}")
    Position getById(@PathVariable Long id) {
        return positionService.findById(id);
    }

    @PostMapping
    ResponseEntity add(@Valid @RequestBody Position position) {
        Position savedPosition = positionService.add(position);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(savedPosition.getId())
                .toUri();

        return ResponseEntity.created(location).build();
    }

    @PutMapping(value = "/{id}")
    ResponseEntity update(@RequestBody Position position,
                          @PathVariable Long id) {
        positionService.update(position, id);

        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(value = "/{id}")
    void delete(@PathVariable Long id) {
        positionService.delete(id);
    }

    @GetMapping(value = "/filter")
    List<Position> filter(@RequestParam(value = "search") String query) {
        return positionService.filter(query);
    }
}
