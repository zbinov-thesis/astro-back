package com.astro.back.services.impl;

import com.astro.back.entities.Tag;
import com.astro.back.repositories.TagRepository;
import com.astro.back.services.interfaces.EventService;
import com.astro.back.services.interfaces.TagService;
import com.github.tennaito.rsql.jpa.JpaCriteriaQueryVisitor;
import cz.jirutka.rsql.parser.RSQLParser;
import cz.jirutka.rsql.parser.ast.RSQLVisitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import java.util.Collections;
import java.util.List;

@Service
public class TagServiceImpl extends BaseServiceImpl<TagRepository, Tag>
        implements TagService {

    private EntityManager entityManager;
    private EventService eventService;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository,
                          EntityManager entityManager,
                          EventService eventService) {
        super(tagRepository);
        this.entityManager = entityManager;
        this.eventService = eventService;
    }

    @Override
    public void delete(Long id) {
        if (!repository.existsById(id)) {
            throw new EntityNotFoundException();
        }

        // could not manage to handle removing non-existing tags with annotations
        eventService.findAll().forEach(event -> {
            event.getTags().remove(repository.getOne(id));
        });

        repository.deleteById(id);
    }

    public List<Tag> filter(String query) {
        RSQLVisitor<CriteriaQuery<Tag>, EntityManager> rsqlVisitor = new JpaCriteriaQueryVisitor<>();
        CriteriaQuery<Tag> criteriaQuery = new RSQLParser().parse(query).accept(rsqlVisitor, entityManager);

        List<Tag> resultList = entityManager.createQuery(criteriaQuery).getResultList();

        return (resultList == null || resultList.isEmpty())
                ? Collections.emptyList()
                : resultList;
    }
}
