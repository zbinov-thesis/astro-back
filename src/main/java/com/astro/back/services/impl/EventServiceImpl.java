package com.astro.back.services.impl;

import com.astro.back.entities.Event;
import com.astro.back.entities.dtos.EventDto;
import com.astro.back.repositories.EventRepository;
import com.astro.back.repositories.TagRepository;
import com.astro.back.services.interfaces.EventService;
import com.astro.back.services.interfaces.ObservationService;
import com.github.tennaito.rsql.jpa.JpaCriteriaQueryVisitor;
import cz.jirutka.rsql.parser.RSQLParser;
import cz.jirutka.rsql.parser.ast.RSQLVisitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EventServiceImpl extends BaseServiceImpl<EventRepository, Event>
        implements EventService {

    private TagRepository tagRepository;
    private EntityManager entityManager;
    private ObservationService observationService;

    @Autowired
    public EventServiceImpl(EventRepository eventRepository,
                            TagRepository tagRepository,
                            EntityManager entityManager,
                            ObservationService observationService) {
        super(eventRepository);
        this.tagRepository = tagRepository;
        this.entityManager = entityManager;
        this.observationService = observationService;
    }

    public List<EventDto> findAllBasic() {
        return repository.findAll(
                Sort.by(Order.asc("startDate"),
                        Order.asc("endDate"),
                        Order.asc("title"),
                        Order.asc("id")))
                .stream()
                .map(EventDto::new)
                .collect(Collectors.toList());
    }

    public Event add(Event event) {
        if (event.getTags() != null) {
            event.getTags().stream()
                    .filter(tag -> tagRepository.findAll().stream()
                            .noneMatch(x -> x.getId().equals(tag.getId())))
                    .forEach(tag -> tagRepository.save(tag));
        }
        return repository.save(event);
    }

    public void delete(Long id) {
        if (!repository.existsById(id)) {
            throw new EntityNotFoundException();
        }

        observationService.filter("event.id==" + id).forEach(observation ->
                observationService.delete(observation.getId())
        );

        repository.deleteById(id);
    }

    public List<Event> filter(String query) {
        RSQLVisitor<CriteriaQuery<Event>, EntityManager> rsqlVisitor = new JpaCriteriaQueryVisitor<>();
        CriteriaQuery<Event> criteriaQuery = new RSQLParser().parse(query).accept(rsqlVisitor, entityManager);

        List<Event> resultList = entityManager.createQuery(criteriaQuery).getResultList();

        return (resultList == null || resultList.isEmpty())
                ? Collections.emptyList()
                : resultList;
    }
}
