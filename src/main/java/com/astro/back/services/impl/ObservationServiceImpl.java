package com.astro.back.services.impl;

import com.astro.back.entities.Observation;
import com.astro.back.repositories.EventRepository;
import com.astro.back.repositories.ObservationRepository;
import com.astro.back.repositories.PositionRepository;
import com.astro.back.services.interfaces.ObservationService;
import com.github.tennaito.rsql.jpa.JpaCriteriaQueryVisitor;
import cz.jirutka.rsql.parser.RSQLParser;
import cz.jirutka.rsql.parser.ast.RSQLVisitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;
import java.security.InvalidParameterException;
import java.util.Collections;
import java.util.List;

@Service
public class ObservationServiceImpl extends BaseServiceImpl<ObservationRepository, Observation>
        implements ObservationService {

    private EventRepository eventRepository;
    private PositionRepository positionRepository;
    private EntityManager entityManager;

    @Autowired
    public ObservationServiceImpl(ObservationRepository observationRepository,
                                  EventRepository eventRepository,
                                  PositionRepository positionRepository,
                                  EntityManager entityManager) {
        super(observationRepository);
        this.eventRepository = eventRepository;
        this.positionRepository = positionRepository;
        this.entityManager = entityManager;
    }

    @Override
    public Observation add(Observation observation) {
        if (observation.getEvent() != null
                && eventRepository.findAll().stream().noneMatch(x -> x.getId().equals(observation.getEvent().getId()))) {
            throw new InvalidParameterException();
        }
        if (observation.getPosition() != null
                && positionRepository.findAll().stream().noneMatch(x -> x.getId().equals(observation.getPosition().getId()))) {
            observation.setPosition(positionRepository.save(observation.getPosition()));
        }

        return repository.save(observation);
    }

    public List<Observation> filter(String query) {
        RSQLVisitor<CriteriaQuery<Observation>, EntityManager> rsqlVisitor = new JpaCriteriaQueryVisitor<>();
        CriteriaQuery<Observation> criteriaQuery = new RSQLParser().parse(query).accept(rsqlVisitor, entityManager);

        List<Observation> resultList = entityManager.createQuery(criteriaQuery).getResultList();

        return (resultList == null || resultList.isEmpty())
                ? Collections.emptyList()
                : resultList;
    }
}
