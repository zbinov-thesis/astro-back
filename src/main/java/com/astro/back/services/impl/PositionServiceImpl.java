package com.astro.back.services.impl;

import com.astro.back.entities.Position;
import com.astro.back.repositories.PositionRepository;
import com.astro.back.services.interfaces.PositionService;
import com.github.tennaito.rsql.jpa.JpaCriteriaQueryVisitor;
import cz.jirutka.rsql.parser.RSQLParser;
import cz.jirutka.rsql.parser.ast.RSQLVisitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;
import java.util.Collections;
import java.util.List;

@Service
public class PositionServiceImpl extends BaseServiceImpl<PositionRepository, Position>
        implements PositionService {

    private EntityManager entityManager;

    @Autowired
    public PositionServiceImpl(PositionRepository positionRepository,
                               EntityManager entityManager) {
        super(positionRepository);
        this.entityManager = entityManager;
    }

    public List<Position> filter(String query) {
        RSQLVisitor<CriteriaQuery<Position>, EntityManager> rsqlVisitor = new JpaCriteriaQueryVisitor<>();
        CriteriaQuery<Position> criteriaQuery = new RSQLParser().parse(query).accept(rsqlVisitor, entityManager);

        List<Position> resultList = entityManager.createQuery(criteriaQuery).getResultList();

        return (resultList == null || resultList.isEmpty())
                ? Collections.emptyList()
                : resultList;
    }
}
