package com.astro.back.services.impl;

import com.astro.back.entities.BaseEntity;
import com.astro.back.services.interfaces.BaseService;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.EntityNotFoundException;
import java.util.List;

public abstract class BaseServiceImpl<TRepository extends JpaRepository<TModel, Long>, TModel extends BaseEntity>
        implements BaseService<TModel> {

    TRepository repository;

    BaseServiceImpl(TRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<TModel> findAll() {
        return repository.findAll();
    }

    @Override
    public TModel findById(Long id) {
        return repository.findById(id)
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public TModel add(TModel entity) {
        return repository.save(entity);
    }

    @Override
    public TModel update(TModel entity, Long id) {
        repository.findById(id)
                .orElseThrow(EntityNotFoundException::new);

        entity.setId(id);

        return repository.save(entity);
    }

    @Override
    public void delete(Long id) {
        if (!repository.existsById(id)) {
            throw new EntityNotFoundException();
        }

        repository.deleteById(id);
    }
}
