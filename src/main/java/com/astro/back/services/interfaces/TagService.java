package com.astro.back.services.interfaces;

import com.astro.back.entities.Tag;

import java.util.List;

public interface TagService extends BaseService<Tag> {

    @Override
    void delete(Long id);

    List<Tag> filter(String query);
}
