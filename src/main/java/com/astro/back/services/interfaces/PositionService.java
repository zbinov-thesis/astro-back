package com.astro.back.services.interfaces;

import com.astro.back.entities.Position;

import java.util.List;

public interface PositionService extends BaseService<Position> {

    List<Position> filter(String query);
}
