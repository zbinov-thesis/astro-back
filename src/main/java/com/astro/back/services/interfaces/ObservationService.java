package com.astro.back.services.interfaces;

import com.astro.back.entities.Observation;

import java.util.List;

public interface ObservationService extends BaseService<Observation> {

    @Override
    Observation add(Observation observation);

    List<Observation> filter(String query);
}
