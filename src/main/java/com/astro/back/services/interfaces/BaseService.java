package com.astro.back.services.interfaces;

import java.util.List;

public interface BaseService<TModel> {

    List<TModel> findAll();

    TModel findById(Long id);

    TModel add(TModel dto);

    TModel update(TModel dto, Long id);

    void delete(Long id);
}
