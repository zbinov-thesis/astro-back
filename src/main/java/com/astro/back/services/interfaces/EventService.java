package com.astro.back.services.interfaces;

import com.astro.back.entities.Event;
import com.astro.back.entities.dtos.EventDto;

import java.util.List;

public interface EventService extends BaseService<Event> {

    List<EventDto> findAllBasic();

    @Override
    Event add(Event event);

    List<Event> filter(String query);
}
