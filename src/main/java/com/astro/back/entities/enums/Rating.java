package com.astro.back.entities.enums;

public enum Rating {
    FULLY_OBSERVABLE,
    HARDLY_OBSERVABLE,
    UNOBSERVABLE;

    @Override
    public String toString() {
        switch (this) {
            case FULLY_OBSERVABLE: {
                return "Fully observable";
            }
            case HARDLY_OBSERVABLE: {
                return "Hardly observable";
            }
            case UNOBSERVABLE: {
                return "Unobservable";
            }
            default: {
                return super.toString();
            }
        }
    }
}
