package com.astro.back.entities;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.sql.Timestamp;
import java.util.Set;

@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Event extends BaseEntity {

    @Column
    private Timestamp startDate;

    @Column
    private Timestamp endDate;

    @Column
    private String title;

    @Column(columnDefinition = "TEXT")
    private String description;

    @ManyToMany
    private Set<Tag> tags;

    @Builder
    public Event(Long id, Timestamp startDate, Timestamp endDate, String title, String description, Set<Tag> tags) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.title = title;
        this.description = description;
        this.tags = tags;
    }
}
