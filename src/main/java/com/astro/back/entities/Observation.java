package com.astro.back.entities;

import com.astro.back.entities.enums.Rating;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.sql.Timestamp;

@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Observation extends BaseEntity {

    @Column
    private String userId;

    @Column
    private Timestamp timestamp;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Event event;

    @OneToOne(orphanRemoval = true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Position position;

    @Column
    private Rating rating;

    @Column(columnDefinition = "TEXT")
    private String specification;

    @Column(columnDefinition = "TEXT")
    private String message;

    @Builder
    public Observation(String userId, Timestamp timestamp, Event event, Position position,
                       Rating rating, String specification, String message) {
        this.userId = userId;
        this.timestamp = timestamp;
        this.event = event;
        this.position = position;
        this.rating = rating;
        this.specification = specification;
        this.message = message;
    }
}
