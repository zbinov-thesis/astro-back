package com.astro.back.entities;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;

@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Tag extends BaseEntity {

    @Column
    private String title;

    @Column(columnDefinition = "TEXT")
    private String description;

    @Builder
    public Tag(String title, String description) {
        this.title = title;
        this.description = description;
    }
}
