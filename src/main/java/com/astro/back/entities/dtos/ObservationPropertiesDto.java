package com.astro.back.entities.dtos;

import com.astro.back.entities.Observation;
import lombok.Data;

import java.text.SimpleDateFormat;

@Data
public class ObservationPropertiesDto {

    private String id;
    private Boolean selected;
    private String timestamp;
    private String rating;
    private String specification;
    private String message;

    public ObservationPropertiesDto(Observation observation) {
        this.id = observation.getId().toString();
        this.selected = false;
        this.timestamp = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm").format(observation.getTimestamp());
        this.rating = observation.getRating().toString();
        this.specification = observation.getSpecification();
        this.message = observation.getMessage();
    }
}
