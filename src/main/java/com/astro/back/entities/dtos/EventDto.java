package com.astro.back.entities.dtos;

import com.astro.back.entities.Event;
import lombok.Data;

@Data
public class EventDto {

    private Long id;
    private String title;
    private String description;

    public EventDto(Event event) {
        this.id = event.getId();
        this.title = event.getTitle();
        this.description = event.getDescription();
    }
}
