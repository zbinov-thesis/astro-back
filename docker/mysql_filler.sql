USE astro;

drop table if exists event;
drop table if exists event_tags;
drop table if exists observation;
drop table if exists position;
drop table if exists tag;
create table event (id bigint not null auto_increment, description TEXT, end_date datetime, start_date datetime, title varchar(255), primary key (id));
create table event_tags (event_id bigint not null, tags_id bigint not null, primary key (event_id, tags_id));
create table observation (id bigint not null auto_increment, message TEXT, rating integer, specification TEXT, timestamp datetime, user_id varchar(255), event_id bigint, position_id bigint, primary key (id));
create table position (id bigint not null auto_increment, altitude double precision, latitude double precision, longitude double precision, primary key (id));
create table tag (id bigint not null auto_increment, description TEXT, title varchar(255), primary key (id));
alter table event_tags add constraint FKbve6i3wh3ugwl1ni0k1a6s9bt foreign key (tags_id) references tag (id);
alter table event_tags add constraint FKhjeq0v70jnlh0glmfoatbqy0b foreign key (event_id) references event (id);
alter table observation add constraint FKgw55u1fph4w0g4iiu8sycrqi3 foreign key (event_id) references event (id);
alter table observation add constraint FKjmmxl0k7ulv2rlvnosp368ctc foreign key (position_id) references position (id);

INSERT INTO tag VALUES -- ID, DESCRIPTION, TITLE
    (1, '', 'moon'),
    (2, 'meteor shower', 'meteor'),
    (3, '', 'eclipse'),
    (4, '', 'equinox'),
    (5, 'best visibility', 'elongation'),
    (6, '', 'conjunction'),
    (7, '', 'sun'),
    (8, '', 'planet');

-- source: http://www.seasky.org/astronomy/astronomy-calendar-2020.html
INSERT INTO event VALUES -- ID, DESCRIPTION, END_DATE, START_DATE, TITLE
    (1, 'The Quadrantids is an above average shower, with up to 40 meteors per hour at its peak. It is thought to be produced by dust grains left behind by an extinct comet known as 2003 EH1, which was discovered in 2003. The shower runs annually from January 1-5. It peaks this year on the night of the 3rd and morning of the 4th. The first quarter moon will set shortly after midnight, leaving fairly dark skies for what could be a good show. Best viewing will be from a dark location after midnight. Meteors will radiate from the constellation Bootes, but can appear anywhere in the sky.',
        '2020-01-04 03:00:00', '2020-01-03 21:00:00', 'Quadrantids Meteor Shower'),
    (2, 'The Moon will be located on the opposite side of the Earth as the Sun and its face will be will be fully illuminated. This phase occurs at 19:23 UTC. This full moon was known by early Native American tribes as the Full Wolf Moon because this was the time of year when hungry wolf packs howled outside their camps. This moon has also been know as the Old Moon and the Moon After Yule.',
        null, '2020-01-10 19:23:00', 'Full Moon'),
    (3, "A penumbral lunar eclipse occurs when the Moon passes through the Earth's partial shadow, or penumbra. During this type of eclipse the Moon will darken slightly but not completely. The eclipse will be visible throughout most of Europe, Africa, Asia, the Indian Ocean, and Western Australia.",
        '2020-01-10 19:53:00', '2020-01-10 19:23:00', 'Penumbral Lunar Eclipse'),
    (4, 'The Moon will located on the same side of the Earth as the Sun and will not be visible in the night sky. This phase occurs at 21:44 UTC. This is the best time of the month to observe faint objects such as galaxies and star clusters because there is no moonlight to interfere.',
        null, '2020-01-24 21:44:00', 'New Moon'),
    (5, 'The Moon will be located on the opposite side of the Earth as the Sun and its face will be will be fully illuminated. This phase occurs at 07:34 UTC. This full moon was known by early Native American tribes as the Full Snow Moon because the heaviest snows usually fell during this time of the year. Since hunting is difficult, this moon has also been known by some tribes as the Full Hunger Moon, since the harsh weather made hunting difficult. This is also the first of four supermoons for 2020. The Moon will be at its closest approach to the Earth and may look slightly larger and brighter than usual.',
        null, '2020-02-09 07:34:00', 'Full Moon, Supermoon'),
    (6, 'The planet Mercury reaches greatest eastern elongation of 18.2 degrees from the Sun. This is the best time to view Mercury since it will be at its highest point above the horizon in the evening sky. Look for the planet low in the western sky just after sunset.',
        '2020-02-17 03:00:00', '2020-02-10 21:00:00', 'Mercury at Greatest Eastern Elongation'),
    (7, 'The Moon will located on the same side of the Earth as the Sun and will not be visible in the night sky. This phase occurs at 15:33 UTC. This is the best time of the month to observe faint objects such as galaxies and star clusters because there is no moonlight to interfere.',
        null, '2020-02-23 15:33:00', 'New Moon'),
    (8, 'The Moon will be located on the opposite side of the Earth as the Sun and its face will be will be fully illuminated. This phase occurs at 17:48 UTC. This full moon was known by early Native American tribes as the Full Worm Moon because this was the time of year when the ground would begin to soften and the earthworms would reappear. This moon has also been known as the Full Crow Moon, the Full Crust Moon, the Full Sap Moon, and the Lenten Moon. This is also the second of four supermoons for 2020. The Moon will be at its closest approach to the Earth and may look slightly larger and brighter than usual.',
        null, '2020-03-09 17:48:00', 'Full Moon, Supermoon'),
    (9, 'The March equinox occurs at 03:50 UTC. The Sun will shine directly on the equator and there will be nearly equal amounts of day and night throughout the world. This is also the first day of spring (vernal equinox) in the Northern Hemisphere and the first day of fall (autumnal equinox) in the Southern Hemisphere.',
        '2020-03-20 03:51:00', '2020-03-20 03:50:00', 'March Equinox'),
    (10, 'The Moon will located on the same side of the Earth as the Sun and will not be visible in the night sky. This phase occurs at 09:29 UTC. This is the best time of the month to observe faint objects such as galaxies and star clusters because there is no moonlight to interfere.',
        null, '2020-03-24 09:29:00', 'New Moon'),
    (11, 'The planet Mercury reaches greatest western elongation of 27.8 degrees from the Sun. This is the best time to view Mercury since it will be at its highest point above the horizon in the morning sky. Look for the planet low in the eastern sky just before sunrise.',
        '2020-03-24 04:40:00', '2020-03-24 04:00:00', 'Mercury at Greatest Western Elongation'),
    (12, 'The planet Venus reaches greatest eastern elongation of 46.1 degrees from the Sun. This is the best time to view Venus since it will be at its highest point above the horizon in the evening sky. Look for the bright planet in the western sky after sunset.',
        '2020-03-24 22:00:00', '2020-03-24 21:00:00', 'Venus at Greatest Eastern Elongation'),
    (13, 'Moon and Mars at a separation of 0.73 degrees.',
        null, '2020-03-18 08:33:00', 'Moon conjunct Mars'),
    (14, 'Jupiter and Mercury at a separation of 1.5 degrees.',
        null, '2020-01-02 16:42:00', 'Jupiter conjuct Mercury');

INSERT INTO event_tags VALUES -- EVENT_ID, TAGS_ID
    (1, 2),
    (2, 1),
    (3, 1),
    (3, 3),
    (4, 1),
    (5, 1),
    (6, 5),
    (7, 1),
    (8, 1),
    (9, 4),
    (9, 7),
    (10, 1),
    (11, 5),
    (12, 5),
    (13, 1),
    (13, 6),
    (13, 8),
    (14, 6),
    (14, 8);

INSERT INTO position VALUES -- ID, ATTITUDE, LATITUDE, LONGITUDE
    (1, 0, 29.2447, 30.9054),
    (2, 0, 50.0291, 20.8829),
    (3, 0, 60.3787, 25.4894),
    (4, 0, 37.1107, -7.6685),
    (5, 0, 51.746692, 19.453503),
    (6, 0, 51.747680, 19.454331);

INSERT INTO observation VALUES -- ID, MESSAGE, RATING, SPECIFICATION, TIMESTAMP, USER_ID, EVENT_ID, POSITION_ID
    (1, "You can clearly see the darkening on the bottom edge of Moon", 0, 'No equipment needed', '2020-01-10 19:31:27', 'qewrii24689', 3, 1),
    (2, "It's so cloudy you can't see sky!", 2, 'Naked eye', '2020-01-10 19:25:01', 'ljgd8548hj3', 3, 2),
    (3, "You can see it, but it's hardly visible due to fog.", 1, 'Dobson 8" F/6 M-CRF', '2020-01-10 19:44:21', '9gjfd57ugj6', 3, 3),
    (4, '', 0, 'Sky-Watcher Synta N-254/1200 Dobson 10" GOTO', '2020-01-10 19:37:56', 'rtuj658ijo8', 3, 4),
    (5, "There's a building blocking the view. Try another spot", 2, 'Levenhuk Skyline PRO 105 MAK', '2019-12-02 11:00:00', '678fg75g6jk', 14, 6),
    (6, "Excelent conditions to observe such event. Splendid!", 0, 'Levenhuk Skyline PRO 105 MAK', '2019-12-02 11:05:00', '678fg75g6jk', 14, 5);
