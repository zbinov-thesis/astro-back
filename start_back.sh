#!/bin/sh

if [ "$1" == "--clean" ] || [ "$2" == "--clean" ];
then
    docker-compose down
#    bash -c "echo 'y' | docker image prune"
    bash -c "docker volume rm $(docker volume ls -qf dangling=true)"
    echo
fi

docker-compose build
#docker-compose up db
bash -c "docker-compose up db" &

while ! docker exec db mysqladmin -uroot -pa ping --silent &> /dev/null ;
do
    echo "Waiting for db to become alive..."
    sleep 2
done

docker-compose up app

if [ "$1" == "--rm" ] || [ "$2" == "--rm" ];
then
    docker-compose down
else
    docker stop db
fi
