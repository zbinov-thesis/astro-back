FROM maven:3.6-ibmjava-8-alpine AS build
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package

FROM openjdk:8-jre-alpine
COPY --from=build /home/app/target/back-0.1.0-SNAPSHOT.jar /home/app/release/back.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/home/app/release/back.jar"]
